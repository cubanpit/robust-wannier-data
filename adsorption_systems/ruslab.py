from sys import argv
from ase.build import hcp0001, add_adsorbate
from ase.constraints import FixAtoms
from ase.optimize.lbfgs import LBFGS
from gpaw import GPAW, FD, Mixer, FermiDirac, restart

tag = 'Ru001'

adsorbate_heights = {'H': 1.0, 'N': 1.108, 'O': 1.257}

slab = hcp0001('Ru',
               size=(2, 2, 4),
               a=2.72,
               c=1.58 * 2.72,
               vacuum=7.0,
               orthogonal=True)
slab.pbc = [True, True, True]
slab.center(axis=2)

if len(argv) > 1:
    adsorbate = argv[1]
    tag = adsorbate + tag
    add_adsorbate(slab, adsorbate, adsorbate_heights[adsorbate], 'hcp')

slab.set_constraint(FixAtoms(mask=slab.get_tags() >= 3))

calc = GPAW(xc='PBE',
            h=0.2,
            mixer=Mixer(0.1, 5, weight=100.0),
            occupations=FermiDirac(width=0.1),
            kpts=[4, 4, 1],
            eigensolver='cg',
            txt=tag + '_relax.txt')
slab.calc = calc

opt = LBFGS(slab, logfile=tag + '.log', trajectory=tag + '.traj')
opt.run(fmax=0.05)
calc.write(tag + '_relax.gpw')

kpts = {'density': 2.0}
calc = GPAW(mode=FD(force_complex_dtype=True),
            kpts=kpts,
            nbands='150%',
            convergence={'bands': 'CBM+4.0'},
            txt=tag + '_scf.txt')
slab.calc = calc
slab.get_potential_energy()
calc.write(tag + '_scf.gpw', mode='all')

kpts = {'density': 5.0, 'gamma': True}
calc = GPAW(tag + '_scf.gpw',
            fixdensity=True,
            kpts=kpts,
            symmetry='off',
            eigensolver='cg',
            txt=tag + '_nscf.txt')
slab.calc = calc
slab.get_potential_energy()
calc.write(tag + '_nscf.gpw', mode='all')

path = slab.cell.bandpath().path.split(sep=',')[0]
bandpath = slab.cell.bandpath(path, density=50)
kpts = bandpath.kpts

calc = GPAW(tag + '_nscf.gpw',
            fixdensity=True,
            symmetry='off',
            kpts=kpts,
            txt=tag + '_bandstructure.txt')
slab.calc = calc
slab.get_potential_energy()
bs = calc.band_structure()
bs.write(tag + '_KSbs.json')
