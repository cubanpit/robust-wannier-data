# Robust Wannier Data

Short summary of the structure of this repository:

- Python script to compute Wannier functions (`testset_materials/test_wannier.py`).
- Python script to compare and plot Kohn-Sham band structure and Wannier interpolated band structure (`testset_materials/compare_bs.py`).
- Python scripts for DFT calculations using ASE and GPAW (`testset_materials/material/calc/*.py`).
- Band structures computed with Kohn-Sham DFT (`*.json`).
- Raw data files, mainly produced by the above mentioned wannierization script (`*.txt`).
- Files about the adsorption and the NV-center system follow the same conventions.

### Needed software

To produce this data we used the following software:

- [ASE](https://wiki.fysik.dtu.dk/ase/) 3.20 with changes to Wannier module from [MR1984](https://gitlab.com/ase/ase/-/merge_requests/1984)
- [GPAW](https://wiki.fysik.dtu.dk/gpaw/) 20.10 with [atomic PAW setups](https://wiki.fysik.dtu.dk/gpaw/setups/setups.html) 0.9.2

Together with following packages to automate, analyse and represent the data:

- [GNU Parallel](https://www.gnu.org/software/parallel/)
- Python + NumPy + Pandas + Matplotlib
- [VESTA](https://jp-minerals.org/vesta/en/)

Some of the features in the `test_wannier.py` script also need [Wannier90](http://www.wannier.org/), but they have not been used to generate the present data.

### License

Any code you can find in this repository is released under MIT license, see LICENSE file for further information.
