#! /usr/bin/env python

# Create structure, relax it and perform a full calculation with GPAW

import sys
import numpy as np
from gpaw import GPAW, FD
from ase.db import connect
from ase.parallel import parprint

if len(sys.argv) == 1:
    # default value for self-consistent calculation
    kptdensity = 5.0
elif len(sys.argv) == 2:
    kptdensity = float(sys.argv[1])
else:
    print("""Provide one argument:
             The density of k-points in the three directions, if no argument
             is provided the default value is used.""")
    exit(1)

seed = 'SnTe2'
db = connect('../../c2db.db')
atoms = db.get_atoms(uid='SnTe2-bf7106f4a58a')
atoms.pbc = (True, True, True)
atoms.center()

kpts = {'density': kptdensity, 'gamma': True}
calc = GPAW(mode=FD(force_complex_dtype=True),
            symmetry='off',
            kpts=kpts,
            xc='PBE',
            eigensolver='cg',
            nbands='300%',
            convergence={'bands': 'CBM+10.0'},
            txt=str(seed) + '_' + str(kptdensity)
                          + '_self-cons.txt')
atoms.calc = calc
atoms.get_potential_energy()

calc.write(str(seed) + '_' + str(kptdensity)
           + '.gpw', mode='all')
