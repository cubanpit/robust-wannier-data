#! /usr/bin/env python

import os
import sys
import subprocess
import argparse
import numpy as np
from time import time
from gpaw import restart
from ase.dft.wannier import Wannier
from ase.io.jsonio import read_json
from ase.dft.kpoints import get_monkhorst_pack_size_and_offset


class cd:
    """Context manager for changing the current working directory"""
    # from https://stackoverflow.com/questions/431684/13197763#13197763

    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def fermi_dirac(E, U=1, T=0.1):
    """Fermi-Dirac distribution.
       'E': energy
       'U': chemical potential
       'T': thermal energy (equal to k_B * temperature)
       Default values are from Psi-k Review on Automated High-Throughput
       Wannierisation (2019)."""

    return 1 / (np.exp((E - U) / T) + 1)


def compare_bs(nwannier, json_bs, cutoff=0, wan=None, wan90=None, batch=False):
    """Compare Kohn-Sham band structure with the Wannier interpolation.
       'wan': ASE Wannier object
       'cutoff': energy threshold for band comparison
       'wan90': Wannier interpolated bandstructure as 1D array of energies
       'json_bs': JSON band structure file exported from ASE
       'batch': disable any print

       Note: Wannier90 band distance has never been tested with magnetic
       materials."""

    if not batch:
        print('Computing band distance using',
              json_bs, 'as reference band structure... ',
              end='', flush=True)
        t = - time()
    # Import KS band structure from file
    bandstructure = read_json(json_bs)
    bandpath = bandstructure.path
    ks_bs = bandstructure.energies[wan.spin]
    ef = bandstructure.reference

    linear_kpts = bandpath.get_linear_kpoint_axis()[0]
    kpts = bandpath.kpts

    # Compute and print Wannier bandstructure
    wan_bs = np.zeros((linear_kpts.shape[0], nwannier))
    if wan is not None:
        with open('WANbands.txt', 'w') as f:
            for k, kpt_c in enumerate(kpts):
                wan_bs[k] = np.linalg.eigvalsh(
                    wan.get_hamiltonian_kpoint(kpt_c)).real
                for eps in wan_bs[k]:
                    print(linear_kpts[k], eps - ef, file=f)
    elif wan90 is not None:
        assert nwannier == int(len(wan90) / len(linear_kpts))
        assert len(wan90) % len(linear_kpts) == 0
        for k, kptc in enumerate(kpts):
            wan_bs[k] = wan90[k*nwannier:(k+1)*nwannier]

    # Compute difference between band structures
    ks_bs = ks_bs[:, :nwannier] - ef
    wan_bs -= ef

    entangled = False
    neg_idx = 0
    for k, kpt_c in enumerate(kpts):
        # index of the last energy band before the cutoff
        neg_idx_new = np.searchsorted(ks_bs[k], cutoff, side='right')
        if neg_idx != neg_idx_new and neg_idx != 0:
            entangled = True
            break
        neg_idx = neg_idx_new

    if entangled:
        # Fermi-Dirac smearing function as in Psi-k Review on Automated
        # High-Throughput Wannierisation (2019)
        smearing = np.sqrt(fermi_dirac(E=ks_bs, U=cutoff)
                           * fermi_dirac(E=wan_bs, U=cutoff))
        bs_dist = (np.sqrt(np.sum((ks_bs - wan_bs)**2 * smearing)
                           / np.sum(smearing)))
        bs_dist_max = np.max(np.abs(ks_bs - wan_bs) * smearing)
    else:
        # keep only bands below the band gap, if there is one
        ks_bs = ks_bs[:, :neg_idx]
        wan_bs = wan_bs[:, :neg_idx]
        bs_dist = np.sqrt(np.sum((ks_bs - wan_bs)**2)) / wan_bs.size
        bs_dist_max = np.max(np.abs(ks_bs - wan_bs))

    if not batch:
        t += time()
        print(f'{t:.1f}s')

    return bs_dist, bs_dist_max


def run_w90(calc, path, nwannier, json_bs, batch=False):
    """Compute Wannier functions with Wannier90
       'calc': GPAW calculator object
       'path': the path for every Wannier90 related file, they are a lot
       'nwannier': number of Wannier functions
       'json_bs': the JSON file to compute band distance (see compare_bs)
       'batch': disable any print"""

    import gpaw.wannier90 as w90

    def total_len(double_list):
        # compute size of the first two dimensions of a list
        return sum([len(a) for a in double_list])

    def get_orbs(calc, binding=True):
        # Return a list of "obitals groups" for each atom.
        # 'binding': only include orbitals with n_j != -1 if True and viceversa
        Na = len(calc.atoms)
        n_ai = []
        for a in range(Na):
            n_ai.append([])
            setup = calc.setups[a]
            for l, n in zip(setup.l_j, setup.n_j):
                if n != -1:
                    if len(n_ai[a]) > 0:
                        n_new = list(np.arange(2 * l + 1)
                                     + n_ai[a][-1][-1] + 1)
                    else:
                        n_new = list(np.arange(2 * l + 1))
                    n_ai[a].append(n_new)
        return n_ai

    def fill_orbs(orbitals_ai, n_ai, nwannier):
        # Fill 'orbitals_ai' with orbitals in 'n_ai' according to
        #  GPAW Wannier90 conventions, in order to have up to 'nwannier'.
        Na = len(orbitals_ai)
        while True:
            prev_len = total_len(orbitals_ai)
            for a in range(Na):
                if total_len(orbitals_ai) < nwannier and len(n_ai[a]) > 0:
                    orbitals_ai[a] += n_ai[a][0]
                    del n_ai[a][0]
                elif total_len(orbitals_ai) > nwannier:
                    for i in range(total_len(orbitals_ai) - nwannier):
                        orbitals_ai[a-1].pop()
                elif total_len(orbitals_ai) == nwannier:
                    break
            if prev_len == total_len(orbitals_ai):
                break
        return orbitals_ai

    label = str(calc.atoms.symbols)

    if not batch:
        print('Computing Wannier functions with Wannier90... ',
              end='', flush=True)
        t = - time()

    bs_dist = None
    bs_dist_max = None

    # Select the orbitals based on the GPAW setup.
    # GPAW Wannier90 receives a list for each atom, the elements of the list
    #  are just indexes of the orbitals as listed in the GPAW setup.
    #
    # EX: Si has the following orbitals in the setup: 3s, 3p, *s, *p *d
    #     The last three have positive energy and have n_j = -1
    #     To select 3s and 3p GPAW Wannier90 expects [0, 1, 2, 3] for Si atom
    Na = len(calc.atoms)
    orbitals_ai = [[] for a in range(Na)]
    n_ai = get_orbs(calc, binding=True)
    orbitals_ai = fill_orbs(orbitals_ai, n_ai, nwannier)
    if total_len(orbitals_ai) < nwannier:
        print('There are not enough binding orbitals (n != -1) in the GPAW',
              'setup, it is not possible to compute Wannier functions with',
              'the GPAW Wannier90 interface. Returning (0, 0, 0).',
              file=sys.stderr)
        return 0, 0, 0

    assert total_len(orbitals_ai) == nwannier

    if not os.path.exists(path):
        os.makedirs(path)

    if json_bs is not None:
        bandstructure = read_json(json_bs)

    with cd(path):
        w90.write_input(calc,
                        bands=range(nwannier),
                        orbitals_ai=orbitals_ai,
                        seed=label,
                        num_iter=1000,
                        plot=True)
        w90.write_wavefunctions(calc, seed=label)
        subprocess.run(['wannier90.x', '-pp', label])

        w90.write_projections(calc, orbitals_ai=orbitals_ai, seed=label)
        w90.write_eigenvalues(calc, seed=label)
        w90.write_overlaps(calc, seed=label)

        if json_bs is not None:
            # specific instruction to enable generic band interpolation
            win_filename = label + '.win'
            with open(win_filename, mode='a') as win_file:
                win_file.write('\ngeninterp = TRUE')

            # write kpt file for generic band interpolation with postw90.x
            path = bandstructure.path
            kpts = path.kpts
            kpt_filename = label + '_geninterp.kpt'
            with open(kpt_filename, mode='w') as kpt_file:
                print(path, file=kpt_file)       # considered as a comment
                print('crystal', file=kpt_file)  # set relative coordinates
                print(len(kpts), file=kpt_file)  # number of kpts
                for i, k in enumerate(kpts):     # list of kpts with index
                    kpt_file.write(str(i+1) + ' ' +
                                   str(k[0]) + ' ' +
                                   str(k[1]) + ' ' +
                                   str(k[2]) + '\n')

        subprocess.run(['wannier90.x', label])

        spreads = []
        read_spreads = False
        wout_filename = label + '.wout'
        with open(wout_filename, mode='r') as wout_file:
            for line in wout_file:
                if read_spreads:
                    if 'WF' in line:
                        spreads.append(float(line.split(')')[1]))
                    else:
                        read_spreads = False
                elif 'Final State' in line:
                    read_spreads = True
                elif 'Final Spread' in line:
                    loc_fun = float(line.split('=')[1])
                    break

        if not batch:
            t += time()
            print(f'{t:.1f}s')

        if json_bs is not None:
            if not batch:
                print('Computing band interpolation and distance',
                      'with Wannier90... ', end='', flush=True)
                t = - time()
            subprocess.run(['postw90.x', label])
            data = np.loadtxt(label + '_geninterp.dat', unpack=True)
            wan_bs = data[4]
            if not batch:
                t += time()
                print(f'{t:.1f}s')

    if json_bs is not None:
        bs_dist, bs_dist_max = compare_bs(wan90=wan_bs,
                                          nwannier=ntot,
                                          json_bs=args.compare_bs,
                                          batch=args.batch)

    return loc_fun, np.array(spreads), bs_dist, bs_dist_max


parser = argparse.ArgumentParser(
        description='Compute Wannier functions from DFT calculation.')
parser.add_argument('-c', '--calculator', dest='calc',
                    help='calculator file')
parser.add_argument('-n', '--number-wannier', dest='ntot',
                    help="total number of Wannier functions (or 'auto')")
group = parser.add_mutually_exclusive_group()
group.add_argument('-fs', '--fixed-states', dest='nfix', type=int,
                   help='number of fixed states')
group.add_argument('-fe', '--fixed-energy', dest='efix', type=float,
                   help="""energy upper limit for fixed states
                           (relative to Fermi level)""")
parser.add_argument('--spin', type=int, default=0,
                    help='select spin channel (default=0)', choices=[0, 1])
parser.add_argument('--initial', default='orbitals',
                    help="""method for initial guess (gaussians, random, bloch,
                            orbitals, scdm), default: orbitals""")
parser.add_argument('--functional', default='std',
                    choices=['std', 'var'],
                    help='spread functional, default: std')
parser.add_argument('--true-random',
                    help='choose a random seed insted of the default one',
                    action='store_true')
parser.add_argument('--nbands', type=int,
                    help='number of bands used for the calculation')
group1 = parser.add_mutually_exclusive_group()
group1.add_argument('--save-pickle',
                    help="""save Wannier functions in given file
                            (single .pickle file)""")
group1.add_argument('--load-pickle',
                    help="""load Wannier functions from given file
                            (single .pickle file)""")
parser.add_argument('--cube',
                    help="""save Wannier functions in given path
                            (many .cube files)""")
parser.add_argument('--no-localize',
                    help="""skip recursive localization and keep initial guess,
                            do not translate before saving to CUBE files""",
                    action='store_true')
parser.add_argument('--best',
                    help="""look for the best total number of Wannier functions
                            to avoid delocalized functions (NB: very slow)""",
                    action='store_true')
parser.add_argument('--compare-bs',
                    help="""compare Kohn-Sham band structure with the one
                            interpolated by Wannier functions, the argument
                            is a JSON file with band structure exported
                            from ASE""")
parser.add_argument('--compare-w90',
                    help="""run Wannier90 on the same system, the argument is
                            the path used for Wannier90 input/output files""")
parser.add_argument('--batch', help='run as batch job, less verbose',
                    action='store_true')
parser.add_argument('--verbose', help='enable Wannier class debug output',
                    action='store_true')
parser.add_argument('--print-header',
                    help='print header for batch data file and exit',
                    action='store_true')

args = parser.parse_args()

if args.print_header:
    print('nbands ntot nfix efix loc_fun spread_mean',
          'spread_stdev spread_min spread_max initial functional',
          'bs_dist bs_dist_max loc_fun_w90 spread_stdev_w90 spread_min_w90',
          'spread_max_w90 bs_dist_w90 bs_dist_max_w90')
    exit(0)
else:
    if args.calc is None or args.ntot is None:
        print('The calculator file and the number of Wannier functions are',
              'required arguments.')
        exit(1)


# load calculator and atoms from file
if not args.batch:
    print('Loading', args.calc, 'calculator file... ',
          end='', flush=True)
    t = - time()
atoms, calc = restart(args.calc, txt=None)
if not args.batch:
    t += time()
    print(f'{t:.1f}s')

if args.ntot != 'auto':
    ntot = int(args.ntot)
else:
    ntot = args.ntot
efix = args.efix
nfix = args.nfix
if nfix == ntot:
    nfix = None
nbands = args.nbands
bs_dist = None
bs_dist_max = None
loc_fun_w90 = None
s_std_w90 = None
s_min_w90 = None
s_max_w90 = None
bs_dist_w90 = None
bs_dist_max_w90 = None

if nfix is not None and type(ntot) is int and ntot < nfix:
    print('You cannot fix more WF than the total number of them.',
          '\nntot =', ntot, '\tnfix =', nfix, file=sys.stderr)
    exit(0)

if nbands is not None and efix is None and nfix is None and not args.batch:
    print('The number of bands will not affect the calculation if there are',
          'no extra degrees of freedom.', '\nnfix =', nfix,
          '\tnbands =', nbands, '\tntot =', ntot, file=sys.stderr)

if nbands is None:
    nbands = calc.get_number_of_bands()

if type(ntot) is int and nbands < ntot:
    print("You cannot get more WF than the number of calculated bands.",
          "\nnbands =", nbands, "\tntot =", ntot, file=sys.stderr)
    exit(0)

if not args.true_random:
    rng = np.random.RandomState(0)
else:
    rng = np.random

if args.load_pickle is None:
    initial = args.initial

    if not args.batch:
        print('Initializing rotation matrix... ',
              end='', flush=True)
        t = - time()
    wan = Wannier(calc=calc,
                  nwannier=ntot,
                  fixedstates=nfix,
                  fixedenergy=efix,
                  initialwannier=initial,
                  functional=args.functional,
                  spin=args.spin,
                  rng=rng,
                  nbands=nbands,
                  verbose=args.verbose)
    if not args.batch:
        t += time()
        print(f'{t:.1f}s')
    if args.best:
        if not args.batch:
            print('Looking for the best total number of Wannier functions... ',
                  end='', flush=True)
            t = - time()
        ntot = wan.get_optimal_nwannier()
        wan = Wannier(calc=calc,
                      nwannier=ntot,
                      fixedstates=nfix,
                      fixedenergy=efix,
                      initialwannier=initial,
                      functional=args.functional,
                      spin=args.spin,
                      rng=rng,
                      nbands=nbands,
                      verbose=args.verbose)
        if not args.batch:
            t += time()
            print(f'{t:.1f}s')
    else:
        ntot = wan.nwannier

    if not args.no_localize:
        if not args.batch:
            print('Maximizing localization of Wannier functions... ',
                  end='', flush=True)
            t = - time()
        wan.localize()
        if not args.batch:
            t += time()
            print(f'{t:.1f}s')
    if args.save_pickle is not None:
        if not args.batch:
            print('Saving rotation and parameter matrices to',
                  args.save_pickle, 'file... ', end='', flush=True)
            t = - time()
        wan.save(args.save_pickle)
        if not args.batch:
            t += time()
            print(f'{t:.1f}s')
else:
    if not args.batch:
        print('Loading rotation and parameter matrices from', args.load_pickle,
              'file... ', end='', flush=True)
        t = - time()
    wan = Wannier(nwannier=ntot,
                  calc=calc,
                  fixedstates=nfix,
                  fixedenergy=efix,
                  file=args.load_pickle,
                  verbose=args.verbose)
    if not args.batch:
        t += time()
        print(f'{t:.1f}s')

if args.compare_bs is not None:
    if efix is not None:
        cutoff = efix
    else:
        cutoff = 0.0
    bs_dist, bs_dist_max = compare_bs(wan=wan,
                                      nwannier=ntot,
                                      cutoff=cutoff,
                                      json_bs=args.compare_bs,
                                      batch=args.batch)

if args.compare_w90 is not None:
    loc_fun_w90, spreads_w90, bs_dist_w90, bs_dist_max_w90 = \
            run_w90(calc=calc, path=args.compare_w90,
                    nwannier=ntot, json_bs=args.compare_bs,
                    batch=args.batch)
    s_std_w90 = np.std(spreads_w90)
    s_min_w90 = np.min(spreads_w90)
    s_max_w90 = np.max(spreads_w90)

loc_fun = wan.get_functional_value()
spreads = wan.get_spreads()

s_mean = np.mean(spreads)
s_std = np.std(spreads)
s_min = np.min(spreads)
s_max = np.max(spreads)

if nfix is None and efix is None:
    nfix = ntot

if args.batch is True:
    print(nbands, ntot, nfix, efix, loc_fun,
          s_mean, s_std, s_min, s_max,
          args.initial, args.functional,
          bs_dist, bs_dist_max, loc_fun_w90,
          s_std_w90, s_min_w90, s_max_w90,
          bs_dist_w90, bs_dist_max_w90)
else:
    sort_idx = np.argsort(spreads)
    print('\nWF', '\tspread')
    for i in sort_idx:
        print(i, f'\t{spreads[i]:.3f}')

    print('\nnbands:', nbands, '\tinitial:', args.initial)
    print('nfix:', nfix, '\tefix:', efix)
    print()
    print('functional:', args.functional, f'\tvalue: {loc_fun:.4f}')
    print(f'mean: {s_mean:.3f}\t\tstd: {s_std:.3f}')
    print(f'min: {s_min:.3f}\t\tmax: {s_max:.3f}')
    if args.compare_bs is not None:
        print()
        print(f'bs_dist: {bs_dist:.4f}\tbs_dist_max: {bs_dist_max:.4f}')
    if args.compare_w90 is not None:
        print()
        print(f'loc_fun_w90: {loc_fun_w90:.4f}\tstd: {s_std_w90:.3f}')
        print(f'min: {s_min_w90:.3f}\t\tmax: {s_max_w90:.3f}')
        if args.compare_bs is not None:
            print(f'bs_dist_w90: {bs_dist_w90:.4f}',
                  f'\tbs_dist_max_w90: {bs_dist_max_w90:.4f}')

if args.cube is not None:
    if args.batch is False:
        print('Saving Wannier functions and their phases in',
              args.cube, 'as CUBE files... ', end='', flush=True)
        t = - time()
    # Translates to a cell that is not on the border of the supercell,
    #  this way the PBC do not affect the image result.
    kptgrid = get_monkhorst_pack_size_and_offset(calc.get_bz_k_points())[0]
    if (kptgrid > 2).all():
        # bulk materials
        wan.translate_all_to_cell(cell=[2, 2, 2])
    elif (kptgrid > 2)[:2].all():
        # thin layers
        wan.translate_all_to_cell(cell=[2, 2, 0])
    # wan.translate_all_to_cell(cell=[0, 0, 0])

    if not os.path.exists(args.cube):
        os.makedirs(args.cube)
    label = str(atoms.symbols)
    for i in range(wan.nwannier):
        wan.write_cube(
            index=i,
            fname=args.cube+'/'+label+'_wf_%i.cube' % i,
            angle=False
        )
        wan.write_cube(
            index=i,
            fname=args.cube+'/'+label+'_wf_%i_phase.cube' % i,
            angle=True
        )
    if args.batch is False:
        t += time()
        print(f'{t:.1f}s')
