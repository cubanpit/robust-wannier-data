#! /usr/bin/env python

# Starting from a self consistent calculation, compute the bandstructure along
#  a path between special points.

import sys
from gpaw import restart
from ase.parallel import parprint

seed = 'Si_bulk'
if len(sys.argv) == 1:
    calc_filename = str(seed)+'.gpw'
elif len(sys.argv) == 2:
    calc_filename = sys.argv[1]
atoms, calc = restart(calc_filename, txt=None)

path = 'GXWKGLUWLK'
bandpath = atoms.cell.bandpath(path, npoints=200)
kpts = bandpath.kpts

calc.set(kpts=kpts,
         fixdensity=True,
         txt=str(seed)+'_bandstructure.txt')
atoms.get_potential_energy()
bs = calc.band_structure()
bs.write(str(seed)+'_KSbs.json')
parprint('Band structure written to JSON file.')
