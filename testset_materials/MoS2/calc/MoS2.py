#! /usr/bin/env python

# Create structure, relax it and perform a full calculation with GPAW

import sys
import os.path
import numpy as np
from ase.io import read
from ase.build import mx2
from gpaw import GPAW, FD
from ase.optimize import QuasiNewton
from gpaw.wavefunctions.pw import PW
from ase.constraints import FixedPlane
from ase.parallel import parprint

if len(sys.argv) == 1:
    # default value for self-consistent calculation
    kptdensity = 5
elif len(sys.argv) == 2:
    kptdensity = float(sys.argv[1])
else:
    print("""Provide one argument:
             The density of k-points in the three directions, if no argument
             is provided the default value is used.""")
    exit(1)

seed = 'MoS2'
traj_filename = str(seed)+'_relax.traj'
if os.path.isfile(traj_filename):
    # import relaxed structure if available
    atoms = read(traj_filename)
else:
    # define MoS2 structure, 'a' and thickness are default from ASE website
    atoms = mx2(formula='MoS2',
                kind='2H',
                a=3.18,
                thickness=3.19,
                size=(1, 1, 1),
                vacuum=5)
    atoms.pbc = (True, True, True)
    atoms.center()

    # relaxation of crystal structure
    kpts = {'size': (8, 8, 2)}
    calc = GPAW(mode=PW(ecut=450),
                kpts=kpts,
                xc='PBE',
                txt=str(seed)+'_relax.txt')
    atoms.calc = calc
    plane = FixedPlane(a=0, direction=(0, 0, 1))
    atoms.set_constraint(plane)
    dyn = QuasiNewton(atoms, trajectory=traj_filename)
    dyn.run(fmax=0.01)

kpts = {'density': kptdensity, 'gamma': True}
calc = GPAW(mode=FD(force_complex_dtype=True),
            symmetry='off',
            kpts=kpts,
            xc='PBE',
            eigensolver='cg',
            nbands=32,
            convergence={'bands': 'CBM+10.0'},
            txt=str(seed) + '_' + str(kptdensity)
                          + '_self-cons.txt')
atoms.calc = calc
atoms.get_potential_energy()

calc.write(str(seed) + '_' + str(kptdensity)
           + '.gpw', mode='all')
