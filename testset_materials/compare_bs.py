#! /usr/bin/env python

# This script plots two bandstructures on the same figure, in order to compare
#  them. It gets the band path and the reference from a JSON file containing
#  a band structure exported by ASE, then it uses two normal text files with
#  band structure coordinates ready to plot. The kpts are expected in "linear"
#  format, with increasing relative distance.

import sys
import argparse
import numpy as np
import matplotlib.pyplot as plt
from ase.io.jsonio import read_json


def compare_bs(nwannier, ef, ks_bs, wan90=None):
    """Compare Kohn-Sham band structure with the Wannier interpolation.
       'nwannier': Number of Wannier functions
       'ef': Fermi energy
       'ks_bs': KS band structure array [Nkpts x Nbands]
       'wan90': Wannier interpolated bandstructure as 1D array of energies
       """

    Nk = ks_bs.shape[0]
    Nb = ks_bs.shape[1]

    # Compute and print Wannier bandstructure
    wan_bs = np.zeros((Nk, nwannier))
    if wan90 is not None:
        assert nwannier == int(len(wan90) / Nk)
        assert len(wan90) % Nk == 0
        for k in range(Nk):
            wan_bs[k] = wan90[k*nwannier:(k+1)*nwannier]

    # Compute difference between band structures
    ks_bs = ks_bs[:, :nwannier] - ef
    wan_bs -= ef

    is_metal = False
    neg_idx = 0
    for k in range(Nk):
        # index of the last negative energy band
        neg_idx_new = np.searchsorted(ks_bs[k], 0, side='right')
        if neg_idx != neg_idx_new and neg_idx != 0:
            is_metal = True
            break
        neg_idx = neg_idx_new

    if is_metal:
        # Fermi-Dirac smearing function as in Psi-k Review on Automated
        # High-Throughput Wannierisation (2019)
        smearing = np.sqrt(fermi_dirac(E=ks_bs) * fermi_dirac(E=wan_bs))
        bs_dist = (np.sqrt(np.sum((ks_bs - wan_bs)**2 * smearing)
                           / np.sum(smearing)))
        bs_dist_max = np.max(np.abs(ks_bs - wan_bs) * smearing)
    else:
        # keep only valence bands for non-metals and compute distance
        ks_bs = ks_bs[:, :neg_idx]
        wan_bs = wan_bs[:, :neg_idx]
        bs_dist = np.sqrt(np.sum((ks_bs - wan_bs)**2)) / wan_bs.size
        bs_dist_max = np.max(np.abs(ks_bs - wan_bs))

    return bs_dist, bs_dist_max


parser = argparse.ArgumentParser(
        description='Plot Kohn-Sham and Wannier interpolated band structures.')
parser.add_argument('--ks', required=True,
                    help='Kohn-Sham band structure in ASE JSON format')
parser.add_argument('--wan', required=True,
                    help='Wannier interpolated band structure in text format')
parser.add_argument('--w90', action='store_true',
                    help='Set Wannier90 mode to interpret geninterp.dat file')
parser.add_argument('--qe', action='store_true',
                    help='Set Quantum ESPRESSO mode to interpret band.gnu file')
parser.add_argument('--spin', default=0, type=int, choices=[0, 1],
                    help="""Plot band structure only for selected spin channel
                            (default=0), Wannier90 band structures are not
                            supported""")

args = parser.parse_args()

if args.qe:
    data = np.loadtxt(args.ks, unpack=True)
    linear_kpts = np.unique(data[0])
    ks_bs = data[1].reshape(-1, linear_kpts.size).T
    ef = 2.3454
else:
    bandstructure = read_json(args.ks)
    bandpath = bandstructure.path
    ks_bs = bandstructure.energies[args.spin]
    ef = bandstructure.reference

    linear_kpts = bandpath.get_linear_kpoint_axis()[0]
    spec_coords = bandpath.get_linear_kpoint_axis()[1]
    spec_names = bandpath.get_linear_kpoint_axis()[2]

fig = plt.figure(1, dpi=80, figsize=(12, 8))
fig.subplots_adjust(left=.1, right=.97, top=.95, bottom=.05)

# Plot Wannier bands
if args.w90:
    data = np.loadtxt(args.wan, unpack=True)
    wan_bs = data[4]
    k = np.repeat(linear_kpts, int(wan_bs.size/linear_kpts.size))
    assert k.size == wan_bs.size
    eps = wan_bs - ef
else:
    k, wan_bs = np.loadtxt(args.wan, unpack=True)
    eps = wan_bs
plt.plot(k, eps, 'o', label='Wannier', ms=3, c='black')
min_wan = min(eps)
max_wan = max(eps)

# Plot KS bands
eps = ks_bs - ef
plt.plot(linear_kpts, eps, '-', lw=1, c='black')

plt.title('Kohn-Sham vs Wannier interpolated band structures - '
          + args.wan, size=16)
plt.plot([min(linear_kpts), max(linear_kpts)],
         [0, 0], 'k:', label='_nolegend_')
# plt.text(-.5, 0, 'Fermi level', ha='left', va='bottom')
plt.axis('tight')
if not args.qe:
     plt.xticks(spec_coords,
                spec_names, size=16)
plt.ylabel(r'$E - E_F\  \rm{[eV]}$', size=16)
plt.xlim(left=np.min(linear_kpts), right=np.max(linear_kpts))
plt.ylim(bottom=min_wan-2, top=max_wan+2)
plt.legend()
plt.show()
