from sys import argv
from ase.build import bulk
from ase.optimize.lbfgs import LBFGS
from gpaw import GPAW, PW, FD, restart

tag = 'NV-diamond'

a = 3.57
atoms = bulk('C',
             crystalstructure='diamond',
             a=a,
             cubic=True)
atoms *= (2, 2, 2)
atoms.numbers[0] = 7
del atoms[1]

calc = GPAW(mode=PW(400),
            kpts=(2, 2, 2),
            charge=-1,
            txt=tag + '_relax.txt')
atoms.calc = calc
opt = LBFGS(atoms, logfile=tag + '.log', trajectory=tag + '.traj')
opt.run(fmax=0.05)
calc.write(tag + '_relax.gpw')

kpts = {'density': 2.0}
calc = GPAW(mode=FD(force_complex_dtype=True),
            kpts=kpts,
            nbands='150%',
            convergence={'bands': 'CBM+4.0'},
            txt=tag + '_scf.txt')
atoms.calc = calc
atoms.get_potential_energy()
calc.write(tag + '_scf.gpw', mode='all')

kpts = {'density': 5.0, 'gamma': True}
calc = GPAW(tag + '_scf.gpw',
            fixdensity=True,
            kpts=kpts,
            symmetry='off',
            eigensolver='cg',
            txt=tag + '_nscf.txt')
atoms.calc = calc
atoms.get_potential_energy()
calc.write(tag + '_nscf.gpw', mode='all')

path = atoms.cell.bandpath().path.split(sep=',')[0]
bandpath = atoms.cell.bandpath(path, density=50)
kpts = bandpath.kpts

calc = GPAW(tag + '_nscf.gpw',
            fixdensity=True,
            symmetry='off',
            kpts=kpts,
            txt=tag + '_bandstructure.txt')
atoms.calc = calc
atoms.get_potential_energy()
bs = calc.band_structure()
bs.write(tag + '_KSbs.json')
